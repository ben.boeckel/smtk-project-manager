09-jan-2019

# To build with cmb:release

```
cmake \
  -C ${cmb-superbuild-build-dir}/cmb-developer-config.cmake \
  -Dsmtk_DIR=${cmb-build-dir}/thirdparty/smtk \
  ${path-to-project_manager-source-dir}
```

Note: use absolute path for smtk_DIR
