//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef project_config_Truchas_h
#define project_config_Truchas_h

#include "Simulation.h"

#include "smtk/PublicPointerDefs.h"

#include <string>

namespace config
{

/// Applies Truchas-specific logic to projects.
class Truchas : public Simulation
{
public:
  /// Apply changes after project created
  bool postCreate(smtk::project::ProjectPtr project) const override;

  /// Apply changes before project export dialog is displayed
  bool preExport(
    smtk::project::ProjectPtr project, smtk::operation::OperationPtr exportOp) const override;

protected:
  bool isInductionHeatingEnabled(smtk::attribute::ResourcePtr& attResource) const;
};

} // namespace config

#endif
