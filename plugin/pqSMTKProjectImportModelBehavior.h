//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqSMTKProjectImportModelBehavior_h
#define pqSMTKProjectImportModelBehavior_h

//#include "Exports.h"

#include "pqReaction.h"

#include <QObject>

/// A reaction for importing a model into a project.
class pqProjectImportModelReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqProjectImportModelReaction(QAction* parent);

  void importModel();

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override { this->importModel(); }

private:
  Q_DISABLE_COPY(pqProjectImportModelReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqSMTKProjectImportModelBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqSMTKProjectImportModelBehavior* instance(QObject* parent = nullptr);
  ~pqSMTKProjectImportModelBehavior() override;

protected:
  pqSMTKProjectImportModelBehavior(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqSMTKProjectImportModelBehavior);
};

#endif
