//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqSMTKProjectExportBehavior.h"

#include "config/Registry.h"
#include "config/Simulation.h"

// SMTK
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ComponentItem.h"
#include "smtk/attribute/DirectoryItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/extension/qt/qtOperationView.h"
#include "smtk/extension/qt/qtUIManager.h"
#include "smtk/io/Logger.h"
#include "smtk/model/Model.h"
#include "smtk/operation/Manager.h"
#include "smtk/project/Manager.h"
#include "smtk/project/Project.h"
#include "smtk/resource/Resource.h"

// Paraview client
#include "pqActiveObjects.h"
#include "pqCoreUtilities.h"
#include "pqServer.h"

// Qt
#include <QAction>
#include <QDebug>
#include <QDialog>
#include <QDir>
#include <QFileInfo>
#include <QIcon>
#include <QMessageBox>
#include <QSharedPointer>
#include <QString>
#include <QTextStream>
#include <QVBoxLayout>
#include <QtGlobal>

#include "nlohmann/json.hpp"

#include <algorithm>  // std::replace
#include <cassert>

using json = nlohmann::json;

#define ALERT_ICON_PATH ":/icons/attribute/errorAlert.png"

//-----------------------------------------------------------------------------
pqProjectExportReaction::pqProjectExportReaction(QAction* parentObject)
  : Superclass(parentObject)
{
}

//-----------------------------------------------------------------------------
void pqProjectExportReaction::exportProject() const
{
  // Access the active server
  pqServer* server = pqActiveObjects::instance().activeServer();

  // Get project manager and export operator
  pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);
  auto projectManager = wrapper->smtkProjectManager();
  auto& logger = smtk::io::Logger::instance();
  bool reset = true;
  auto exportOp = projectManager->getExportOperator(logger, reset);
  if (exportOp == nullptr)
  {
    QMessageBox::warning(pqCoreUtilities::mainWidget(), tr("Failed Getting Export Operator"),
      tr(logger.convertToString().c_str()));
    return;
  }

  auto project = projectManager->getCurrentProject();

  // Apply any simulation-specific initialization
  //exportOp->configure();  // future
  std::string simCode = project->simulationCode();
  smtk::shared_ptr<config::Simulation> config = config::Registry::getConfig(simCode.c_str());
  // qDebug() << "sim-config:" << !!config;
  if (config != nullptr)
  {
    config->preExport(project, exportOp);
  }
  else
  {
    qDebug() << "No config instance for" << QString::fromStdString(simCode);
  }

  // Construct a modal dialog for the operation.
  auto exportDialog = new QDialog(pqCoreUtilities::mainWidget());
  exportDialog->setObjectName("SimulationExportDialog");
  exportDialog->setWindowTitle("Simulation Export Dialog");
  exportDialog->setLayout(new QVBoxLayout(exportDialog));
  // TODO: the dialog size should not be set this way. It should auto-expand
  // to accommodate the contained opView. Either Qt is being coy, or smtk's
  // qtBaseView logic for resizing doesn't inform the containing parent of its
  // decisions.
  exportDialog->resize(600, 300);

  // Create a new UI for the dialog.
  auto uiManager = new smtk::extension::qtUIManager(exportOp, wrapper->smtkResourceManager());

  // Create an operation view for the operation.
  smtk::view::ViewPtr view = uiManager->findOrCreateOperationView();
  smtk::extension::qtOperationView* opView =
    dynamic_cast<smtk::extension::qtOperationView*>(uiManager->setSMTKView(view, exportDialog));

  exportDialog->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

  // Alert the user if the operation fails. Close the dialog if the operation
  // succeeds.
  smtk::operation::Operation::Result result;
  connect(opView, &smtk::extension::qtOperationView::operationExecuted,
    [=](const smtk::operation::Operation::Result& result) {
      if (result->findInt("outcome")->value() !=
        static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
      {
        QMessageBox msgBox(pqCoreUtilities::mainWidget());
        msgBox.setStandardButtons(QMessageBox::Ok);
        // Create a spacer so it doesn't look weird
        QSpacerItem* horizontalSpacer =
          new QSpacerItem(300, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);
        msgBox.setText("Export failed. Please see output log for more details.");
        QGridLayout* layout = (QGridLayout*)msgBox.layout();
        layout->addItem(horizontalSpacer, layout->rowCount(), 0, 1, layout->columnCount());
        msgBox.exec();

        // Once the user has accepted that their export failed, they are
        // free to try again without changing any options.
        opView->onModifiedParameters();
      }
      else
      {
        qInfo() << "Export operation completed";
        exportDialog->done(QDialog::Accepted);

        // Check log for warnings
        int numWarnings = 0;
        auto logItem = result->findString("log");
        std::string logString = logItem->value(0);
        if (!logString.empty())
        {
          auto jsonLog = json::parse(logString);
          assert(jsonLog.is_array());
          for (json::iterator it = jsonLog.begin(); it != jsonLog.end(); ++it)
          {
            auto jsRecord = *it;
            assert(jsRecord.is_object());
            if (jsRecord["severity"] >= static_cast<int>(smtk::io::Logger::WARNING))
            {
              numWarnings++;
              std::string content = jsRecord["message"];
              // Escape any quote signs (formats better)
              std::replace(content.begin(), content.end(), '"', '\"');
              qWarning("%d. %s", numWarnings, content.c_str());
            } // if (>= warning)
          } // for (it)
        } // if (logString)

        qDebug() << "Number of warnings:" << numWarnings;

        if (numWarnings > 0)
        {
          QWidget *parentWidget = pqCoreUtilities::mainWidget();
          QString text;
          QTextStream qs(&text);
          qs << "WARNING: The generated file is INCOMPLETE or INVALID."
             << " You can find more details in the Output Messages view."
             << " You will generally need to correct all invalid input fields"
             << " in the Attribute Editor in order to generate a valid Truchas"
             << " input file."
             << " Look in the Attribute Editor panel for red \"alert\" icons"
             << " and input fields with red background."
             << "\n\nNumber of errors: " << numWarnings;
          QMessageBox msgBox(
            QMessageBox::NoIcon, "Export Warnings", text, QMessageBox::NoButton, parentWidget);
          msgBox.setIconPixmap(QIcon(ALERT_ICON_PATH).pixmap(32,32));
          msgBox.exec();
        }
      } // else
    });

  // Launch the modal dialog and wait for the operation to succeed.
  exportDialog->exec();
  delete uiManager;
  delete exportDialog;
} // exportProject()

//-----------------------------------------------------------------------------
static pqSMTKProjectExportBehavior* g_instance = nullptr;

pqSMTKProjectExportBehavior::pqSMTKProjectExportBehavior(QObject* parent)
  : Superclass(parent)
{
}

pqSMTKProjectExportBehavior* pqSMTKProjectExportBehavior::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new pqSMTKProjectExportBehavior(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

pqSMTKProjectExportBehavior::~pqSMTKProjectExportBehavior()
{
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  QObject::disconnect(this);
}
