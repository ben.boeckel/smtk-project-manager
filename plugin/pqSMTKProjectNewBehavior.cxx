//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqSMTKProjectNewBehavior.h"

#include "config/Registry.h"
#include "config/Simulation.h"

// SMTK
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/DirectoryItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/Resource.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/extension/qt/qtBaseView.h"
#include "smtk/extension/qt/qtInstancedView.h"
#include "smtk/extension/qt/qtUIManager.h"
#include "smtk/io/AttributeWriter.h"
#include "smtk/io/Logger.h"
#include "smtk/operation/Manager.h"
#include "smtk/operation/Manager.h"
#include "smtk/project/Manager.h"
#include "smtk/project/Project.h"
#include "smtk/view/View.h"

// Client side
#include "pqActiveObjects.h"
#include "pqCoreUtilities.h"
#include "pqServer.h"
#include "vtkSMProperty.h"
#include "vtkSMProxy.h"
#include "vtkSMSessionProxyManager.h"
#include "vtkSMStringVectorProperty.h"

#include <QAction>
#include <QDebug>
#include <QDialog>
#include <QDialogButtonBox>
#include <QMessageBox>
#include <QPushButton>
#include <QSharedPointer>
#include <QSizePolicy>
#include <QString>
#include <QVBoxLayout>
#include <QtGlobal>

#include "boost/filesystem.hpp"

#include <string>
#include <vector>

//-----------------------------------------------------------------------------
pqProjectNewReaction::pqProjectNewReaction(QAction* parentObject)
  : Superclass(parentObject)
{
}

//-----------------------------------------------------------------------------
void pqProjectNewReaction::newProject()
{
  // Access the active server
  pqServer* server = pqActiveObjects::instance().activeServer();
  pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);

  // Get project specification
  auto projectManager = wrapper->smtkProjectManager();
  auto spec = projectManager->getProjectSpecification();
  auto specResource = spec->attributeResource();

  // Check settings for ProjectsRootFolder & WorkflowsFolder
  vtkSMProxy* proxy = server->proxyManager()->GetProxy("settings", "SMTKSettings");
  if (proxy)
  {
    vtkSMProperty* projectProp = proxy->GetProperty("ProjectsRootFolder");
    auto projectStringProp = vtkSMStringVectorProperty::SafeDownCast(projectProp);
    if (projectStringProp)
    {
      std::string projectFolder = projectStringProp->GetElement(0);
      if (!projectFolder.empty())
      {
        auto folderItem = spec->findDirectory("workspace-path");
        if (folderItem)
        {
          folderItem->setValue(0, projectFolder);
        }
      }
    } // if (projectStringProp)

    vtkSMProperty* workflowProp = proxy->GetProperty("WorkflowsFolder");
    auto workflowStringProp = vtkSMStringVectorProperty::SafeDownCast(workflowProp);
    if (workflowStringProp)
    {
      std::string workflowFolder = workflowStringProp->GetElement(0);
      if (!workflowFolder.empty())
      {
        // Check for sbt file
        boost::filesystem::path workflowPath(workflowFolder);
        if (boost::filesystem::exists(workflowPath))
        {
          std::vector<boost::filesystem::path> sbtFiles;
          boost::filesystem::directory_iterator iter(workflowPath);
          boost::filesystem::directory_iterator endit;
          for (; iter != endit; ++iter)
          {
            if (boost::filesystem::is_regular_file(*iter) && iter->path().extension() == ".sbt")
            {
              sbtFiles.push_back(iter->path().string());
            }
          } // for (iter)

          auto fileItem = spec->findFile("simulation-template");
          if (!!fileItem && sbtFiles.size() == 1)
          {
            fileItem->setValue(0, sbtFiles[0].string());
          }

        } // if (workflowPath)
      }
    }
  }

  // Construct a modal dialog for the new project spec
  QSharedPointer<QDialog> dialog =
    QSharedPointer<QDialog>(new QDialog(pqCoreUtilities::mainWidget()));
  dialog->setObjectName("NewProjectDialog");
  dialog->setWindowTitle("Specify New Project");
  dialog->setLayout(new QVBoxLayout(dialog.data()));

  // Create the ui manager
  QSharedPointer<smtk::extension::qtUIManager> uiManager =
    QSharedPointer<smtk::extension::qtUIManager>(new smtk::extension::qtUIManager(specResource));

  // Create the SMTK view
  auto view = specResource->findTopLevelView();
  auto qtView = uiManager->setSMTKView(view, dialog.data());
  auto instancedView = dynamic_cast<smtk::extension::qtInstancedView*>(qtView);
  QObject::connect(instancedView, &smtk::extension::qtInstancedView::modified, this,
    &pqProjectNewReaction::onModifiedParameters);

  // Add cancel & apply buttons
  auto buttonBox = new QDialogButtonBox(QDialogButtonBox::Cancel | QDialogButtonBox::Apply);
  dialog->layout()->addWidget(buttonBox);

  QPushButton* applyButton = buttonBox->button(QDialogButtonBox::Apply);
  applyButton->setEnabled(instancedView->isValid());
  applyButton->setDefault(true);
  QObject::connect(this, &pqProjectNewReaction::dialogValid, applyButton, &QPushButton::setEnabled);
  QObject::connect(applyButton, &QPushButton::clicked, dialog.data(), &QDialog::accept);
  QObject::connect(buttonBox, &QDialogButtonBox::rejected, dialog.data(), &QDialog::reject);

  // Run the dialog
  dialog->setMinimumWidth(300);
  dialog->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
  if (dialog->exec() == QDialog::Accepted)
  {
    // Call the newProject method
    bool replaceExistingDirectory = false;
    auto& logger = smtk::io::Logger::instance();
    auto project = projectManager->createProject(spec, replaceExistingDirectory, logger);
    if (!project)
    {
      std::string msg = logger.convertToString();
      if (msg.empty())
      {
        msg = "Internal error creating project";
      }
      QMessageBox::warning(
        pqCoreUtilities::mainWidget(), tr("Failed To Create Project"), tr(msg.c_str()));
      return;
    }

    // Apply any simulation-specific initialization
    auto simCode = project->simulationCode();
    smtk::shared_ptr<config::Simulation> config = config::Registry::getConfig(simCode.c_str());
    qDebug() << "sim-config:" << !!config;
    if (config != nullptr)
    {
      config->postCreate(project);
    }

    qInfo() << "Created project" << project->name().c_str();
    qInfo() << "Be sure to save this project before closing it";
    emit this->projectCreated(project);
  } // if
} // newProject()

//-----------------------------------------------------------------------------
void pqProjectNewReaction::onModifiedParameters()
{
  auto instancedView = dynamic_cast<smtk::extension::qtInstancedView*>(QObject::sender());
  //qDebug() << "onModifiedParameters(), " << instancedView->isValid();
  emit this->dialogValid(instancedView->isValid());
}

//-----------------------------------------------------------------------------
static pqSMTKProjectNewBehavior* g_instance = nullptr;

pqSMTKProjectNewBehavior::pqSMTKProjectNewBehavior(QObject* parent)
  : Superclass(parent)
{
}

pqSMTKProjectNewBehavior* pqSMTKProjectNewBehavior::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new pqSMTKProjectNewBehavior(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

pqSMTKProjectNewBehavior::~pqSMTKProjectNewBehavior()
{
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  QObject::disconnect(this);
}
